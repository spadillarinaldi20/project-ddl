CREATE TABLE ROLE_TYPES (
    role_codes VARCHAR(1),
    role VARCHAR(255),
    PRIMARY KEY (role_codes)
);

CREATE TABLE CATEGORIES (
    c_code VARCHAR(4),
    category VARCHAR(255),
    PRIMARY KEY (c_code)
);

CREATE TABLE PEOPLE (
    p_ref VARCHAR(255),
    p_code VARCHAR(7), --Writer (W), Actor (A), Cinematographer C), Visual or art director (V), coMposer (M), choreoGrapher (G), some Book authors.(B)
    p_id VARCHAR(3),
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    birth_year INT,
    death_year INT,
    notes VARCHAR(255),
    PRIMARY KEY (p_ref)
);

CREATE TABLE COLOR_CODES (
     code VARCHAR(255),
     Full_name VARCHAR(255),
     Description VARCHAR(255),
     PRIMARY KEY (code)
);

CREATE TABLE GEOGRAPHY (
   ct_code VARCHAR(255),
   Country VARCHAR(255),
   adjective VARCHAR(255),
   PRIMARY KEY (ct_code)
);

CREATE TYPE AWARD_TYPES AS (
   agency VARCHAR(255),
   award_name VARCHAR(255),
   award_year INT
);

CREATE TABLE ACTORS (
    stage_name VARCHAR(255),
    dowstrt VARCHAR(255),
    dowend VARCHAR(255),
    family_name_at_birth VARCHAR(255),
    birth_name VARCHAR(255),
    origin VARCHAR(255), --Foreign key for ctcode in categories
    gender VARCHAR(1),
    dob DATE,
    dod DATE,
    role_type VARCHAR(1),
    picture VARCHAR(255),
    notes VARCHAR(255),
    PRIMARY KEY (stage_name),
    FOREIGN KEY (origin) REFERENCES GEOGRAPHY(ct_code),
    FOREIGN KEY (role_type) REFERENCES ROLE_TYPES(role_codes)
);

CREATE TABLE STUDIOS (
	name VARCHAR(255),
	company VARCHAR(255),
    --Not sure if we want this a GEOGRAPHY or not, so i have both
    geography VARCHAR(255),
    --or
	-- city VARCHAR(255),
	-- country VARCHAR(255),
	fddate DATE,--im assuming this is the start date
	enddate DATE,
	founder VARCHAR(255),
	successor VARCHAR(255),
	notes VARCHAR(255),
	PRIMARY KEY (name),
	FOREIGN KEY (founder) REFERENCES PEOPLE(p_ref), --not sure if we want to fkey here
	--only if using geography
	FOREIGN KEY (geography) REFERENCES GEOGRAPHY(ct_code)
);

CREATE TABLE MOVIES (
    film_id VARCHAR(255),
    title VARCHAR(255),
    director VARCHAR(255),
    producers VARCHAR(255),
    year INT,
    locale VARCHAR(255),
    studio VARCHAR(255),
    prc VARCHAR(255), --not sure what this is
    category VARCHAR(4),
    notes VARCHAR(255),
    PRIMARY KEY (film_id),
    FOREIGN KEY (director) REFERENCES PEOPLE(p_ref),
    FOREIGN KEY (producers) REFERENCES PEOPLE(p_ref),
    FOREIGN KEY (studio) REFERENCES STUDIOS(name),
    FOREIGN KEY (category) REFERENCES CATEGORIES(c_code)
);

CREATE TABLE CASTS (
	film_id VARCHAR(16), --may need to make this bigger
	title VARCHAR(255),
	actor VARCHAR(255),
	role VARCHAR(255),
	roletype VARCHAR(1), --references the roles in PERSON(p_code)?
	notes VARCHAR(255),
	PRIMARY KEY (film_id),
	FOREIGN KEY (film_id) REFERENCES MOVIES(film_id),
    FOREIGN KEY (actor) REFERENCES ACTORS(stage_name)
);

CREATE TABLE REMAKES (
-- filmid <TH> title <TH> year <TH> fraction<TH> priorfilm <TH> priortitle <TH> prioryear<TH>
-- Is fractions needed? 
    filmid VARCHAR(16),
    title VARCHAR(255),
    year DATE,
    fraction FLOAT,
    priorfilm VARCHAR(16),
    priortitle VARCHAR(255),
    prioryear DATE,
	PRIMARY KEY (filmid),
	FOREIGN KEY (priorfilm) REFERENCES MOVIES(film_id)
);

CREATE TABLE AWARDS (
	award_id INT, --could use just a int as a primary "index"
	film_id VARCHAR(255), --think its best to have the film linked this way
	award_Type AWARD_TYPES,
	reason VARCHAR(255),
	place VARCHAR(255),
	receipent VARCHAR(255),
	notes VARCHAR(255),
	PRIMARY KEY (film_id), --just an option, im not set on it at all
	FOREIGN KEY (film_id) REFERENCES MOVIES(film_id)
);


