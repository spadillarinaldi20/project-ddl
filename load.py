# python 3.9
import os.path
import shutil
import urllib.request
import zipfile
import psycopg2 # https://www.psycopg.org/docs/usage.html

GITHUB_DATA_LINK = "https://github.com/cernoch/movies/archive/refs/heads/master.zip"

db = psycopg2.connect(database="project",
                        host="localhost",
                        port="5432")


def main():
    print("Please make sure the Database is cleared, as this script will re-initalize it")
    input("Press ENTER to continue...")

    print("Cleaning Working Dir")
    if os.path.isdir("./work"):
        shutil.rmtree("./work")
    os.mkdir("./work")

    print("Downloading Data")
    urllib.request.urlretrieve(GITHUB_DATA_LINK, "./work/master.zip")
    # i guess technically i should verify the hash, but someone else can do that cause i dont wanna

    print("Unzipping Data")
    os.mkdir("./work/master")
    with zipfile.ZipFile("./work/master.zip", 'r') as zip_ref:
        zip_ref.extractall("./work/master/")

    print("Running DDL file")
    with db.cursor() as cur:
        cur.execute(open("schema.sql", "r").read())
        db.commit()

    print("Load Tables")


if __name__ == '__main__':
    main()